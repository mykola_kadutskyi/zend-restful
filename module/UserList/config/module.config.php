<?php

namespace UserList;

return array(
	'controllers'  => array(
		'invokables' => array(
			'UserList\Controller\UserApiController' => 'UserList\Controller\UserApiController',
			'UserList\Controller\UserRestfulApiController' => 'UserList\Controller\UserRestfulApiController',
		),
	),
	'router' => array(
        'routes' => array(
            'userlist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/userlist[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'UserList\Controller\UserApiController',
                        'action' => 'get',
                    ),
                ),
            ),
            'user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api[/:id]',
                    'constraints' => array(
	                    'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'UserList\Controller\UserRestfulApiController',
                    ),
                ),
            ),
        ),
    ),
	'view_manager' => array(
		'template_path_stack' => array(
			'user' => __DIR__ . '/../view',
		),
	),
	'doctrine'     => array(
		'fixture' => array(
			__NAMESPACE__ . '_fixture' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
		),
		'driver'  => array(
			__NAMESPACE__ . '_driver' => array(
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array( __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity' )
			),
			'orm_default'             => array(
				'drivers' => array(
					__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
				)
			)
		),
	),
);