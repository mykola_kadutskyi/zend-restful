<?php

namespace UserList\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * A user.
 *
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @property string $userName
 * @property string $fullName
 * @property string $email
 * @property int $phoneNumber
 * @property string $address
 * @property int $id
 * @ORM\EntityListeners({"UserList\Listener\User"})
 */
class User {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer");
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	/**
	 * @ORM\Column(type="string")
	 */
	protected $userName;
	/**
	 * @ORM\Column(type="string")
	 */
	protected $fullName;
	/**
	 * @ORM\Column(type="string")
	 */
	protected $email;
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $phoneNumber;
	/**
	 * @ORM\Column(type="string")
	 */
	protected $address;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUserName() {
		return $this->userName;
	}

	/**
	 * @param mixed $userName
	 */
	public function setUserName( $userName ) {
		$this->userName = $userName;
	}

	/**
	 * @return mixed
	 */
	public function getFullName() {
		return $this->fullName;
	}

	/**
	 * @param mixed $fullName
	 */
	public function setFullName( $fullName ) {
		$this->fullName = $fullName;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPhoneNumber() {
		return $this->phoneNumber;
	}

	/**
	 * @param mixed $phoneNumber
	 */
	public function setPhoneNumber( $phoneNumber ) {
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * @return mixed
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param mixed $address
	 */
	public function setAddress( $address ) {
		$this->address = $address;
	}

	public function toArray() {
		return array(
			'id'          => $this->getId(),
			'userName'    => $this->getUserName(),
			'fullName'    => $this->getFullName(),
			'email'       => $this->getEmail(),
			'phoneNumber' => $this->getPhoneNumber(),
			'address'     => $this->getAddress(),
		);
	}


}