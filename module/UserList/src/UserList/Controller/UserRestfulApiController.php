<?php

namespace UserList\Controller;

use Doctrine\ORM\Query;
use UserList\Entity\User;
use Zend\Http\PhpEnvironment\Request;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Zend\Serializer\Serializer;


class UserRestfulApiController extends AbstractRestfulController {
	/**
	 * @var DoctrineORMEntityManager
	 */
	protected $em;

	/**
	 * @return array|object|DoctrineORMEntityManager
	 */
	public function getEntityManager() {
		if ( null === $this->em ) {
			$this->em = $this->getServiceLocator()->get( 'doctrine.entitymanager.orm_default' );
		}

		return $this->em;
	}

	/**
	 * @param mixed $id
	 *
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function get( $id ) {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isGet() ) {
			$user = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) );
			if ( ! $user ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'couldn\'t find user with id: ' . $id,
				) ) );
			} else {
				$user = $user->toArray();
				$response->setContent( Json::encode( array( 'response' => true, 'user' => $user ) ) );
			}
		}


		return $response;
	}

	/**
	 * @param mixed $data
	 *
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function create( $data ) {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPost() ) {
			$user = new User();
			$user->setUserName( $data['userName'] );
			$user->setFullName( $data['fullName'] );
			$user->setEmail( $data['email'] );
			$user->setPhoneNumber( $data['phoneNumber'] );
			$user->setAddress( $data['address'] );

			$em = $this->getEntityManager();
			$em->persist( $user );
			$em->flush();
			$id = $user->getId();

			if ( ! $id ) {
				$response->setContent( Json::encode( array( 'response' => false ) ) );
			} else {
				$response->setContent( Json::encode( array( 'response' => true, 'user_id' => $id ) ) );
			}
		}

		return $response;
	}

	/**
	 * @param mixed $id
	 *
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function delete( $id ) {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isDelete() ) {

			$em = $this->getEntityManager();

			if ( ! $id ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'user id should be provided',
				) ) );
			} else if ( ! $user = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'couldn\'t find user with id: ' . $id,
				) ) );
			} else {
				$em->remove( $user );
				$em->flush();


				if ( ! $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
					$response->setContent( Json::encode( array( 'response' => true ) ) );
				} else {
					$response->setContent( Json::encode( array( 'response' => false ) ) );
				}
			}
		}

		return $response;
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function getList() {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isGet() ) {

			$users = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findAll();
			foreach ( $users as $key => $usr ) {
				$users[ $key ] = $usr->toArray();
			}
			$response->setContent( Json::encode( array(
				'response' => true,
				'users'    => $users,
			) ) );
		}

		return $response;
	}

	/**
	 * @param mixed $id
	 * @param mixed $data
	 *
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function update( $id, $data ) {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPut() ) {
			$user_content = $data;

			$em = $this->getEntityManager();

			if ( ! $id ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'user id should be provided',
				) ) );
			} else if ( ! $user = $em->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'couldn\'t find user with id: ' . $id,
				) ) );
			} else {


				$user->setUserName( $user_content['userName'] );
				$user->setFullName( $user_content['fullName'] );
				$user->setEmail( $user_content['email'] );
				$user->setPhoneNumber( $user_content['phoneNumber'] );
				$user->setAddress( $user_content['address'] );

				$em = $this->getEntityManager();
				$em->persist( $user );
				$em->flush();

				if ( $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
					$response->setContent( Json::encode( array( 'response' => true ) ) );
				} else {
					$response->setContent( Json::encode( array( 'response' => false ) ) );
				}
			}
		}

		return $response;
	}


}