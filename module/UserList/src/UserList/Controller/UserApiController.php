<?php

namespace UserList\Controller;

use Doctrine\ORM\Query;
use UserList\Entity\User;
use Zend\Http\PhpEnvironment\Request;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Zend\Serializer\Serializer;


class UserApiController extends AbstractActionController {
	/**
	 * @var DoctrineORMEntityManager
	 */
	protected $em;

	/**
	 * @return array|object|DoctrineORMEntityManager
	 */
	public function getEntityManager() {
		if ( null === $this->em ) {
			$this->em = $this->getServiceLocator()->get( 'doctrine.entitymanager.orm_default' );
		}

		return $this->em;
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function getAction() {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPost() ) {
			$data = $request->getPost();
			$id   = $data['id'];

			if ( ! $id ) {
				$users = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findAll();
				foreach ( $users as $key => $usr ) {
					$users[ $key ] = $usr->toArray();
				}
				$response->setContent( Json::encode( array(
					'response' => true,
					'users'    => $users,
				) ) );
			} else {
				if ( ! $user = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) )->toArray() ) {
					$response->setContent( Json::encode( array(
						'response' => false,
						'error'    => 'couldn\'t find user with id: ' . $id,
					) ) );
				} else {
					$response->setContent( Json::encode( array( 'response' => true, 'user' => $user ) ) );
				}
			}

		}

		return $response;
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function addAction() {
		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPost() ) {
			$user = new User();
			$user->setUserName( $request->getPost( 'userName', null ) );
			$user->setFullName( $request->getPost( 'fullName', null ) );
			$user->setEmail( $request->getPost( 'email', null ) );
			$user->setPhoneNumber( $request->getPost( 'phoneNumber', null ) );
			$user->setAddress( $request->getPost( 'address', null ) );

			$em = $this->getEntityManager();
			$em->persist( $user );
			$em->flush();
			$id = $user->getId();

			if ( ! $id ) {
				$response->setContent( Json::encode( array( 'response' => false ) ) );
			} else {
				$response->setContent( Json::encode( array( 'response' => true, 'user_id' => $id ) ) );
			}
		}

		return $response;
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function removeAction() {

		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPost() ) {
			$data = $request->getPost();
			$id   = $data['id'];
			$em   = $this->getEntityManager();

			if ( ! $id ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'user id should be provided',
				) ) );
			} else if ( ! $user = $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'couldn\'t find user with id: ' . $id,
				) ) );
			} else {
				$em->remove( $user );
				$em->flush();


				if ( ! $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
					$response->setContent( Json::encode( array( 'response' => true ) ) );
				} else {
					$response->setContent( Json::encode( array( 'response' => false ) ) );
				}
			}
		}

		return $response;
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function updateAction() {

		$request  = $this->getRequest();
		$response = $this->getResponse();

		if ( $request->isPost() ) {
			$data         = $request->getPost();
			$id           = $data['id'];
			$user_content = $data['content'];

			$em = $this->getEntityManager();

			if ( ! $id ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'user id should be provided',
				) ) );
			} else if ( ! $user = $em->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
				$response->setContent( Json::encode( array(
					'response' => false,
					'error'    => 'couldn\'t find user with id: ' . $id,
				) ) );
			} else {


				$user->setUserName( $user_content['userName'] );
				$user->setFullName( $user_content['fullName'] );
				$user->setEmail( $user_content['email'] );
				$user->setPhoneNumber( $user_content['phoneNumber'] );
				$user->setAddress( $user_content['address'] );

				$em = $this->getEntityManager();
				$em->persist( $user );
				$em->flush();

				if ( $this->getEntityManager()->getRepository( 'UserList\Entity\User' )->findOneBy( array( 'id' => $id ) ) ) {
					$response->setContent( Json::encode( array( 'response' => true ) ) );
				} else {
					$response->setContent( Json::encode( array( 'response' => false ) ) );
				}
			}
		}

		return $response;

	}
}